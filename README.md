# Web personal - Jose Luis Orozco Mejia

Este proyecto utiliza Angular 6 y Firebase

## Autor: Jose Luis Orozco

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Ejecutar `ng build` para compilar el proyecto

## Running unit tests

Ejecutar `ng test` oara ejecutar unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Ejecutar `ng e2e` tpara ejecutar end-to-end tests via [Protractor](http://www.protractortest.org/).
