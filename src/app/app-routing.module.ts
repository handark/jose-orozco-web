import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { StudiesComponent } from './components/studies/studies.component';
import { ExperienceProjectsComponent } from './components/experience-projects/experience-projects.component';
import { TecnologyComponent } from './components/tecnology/tecnology.component';
import { DisplayAlegraComponent } from './components/display-alegra/display-alegra.component';

const routes: Routes = [
  { 
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  { path: 'studies', component: StudiesComponent },
  { path: 'experience', component: ExperienceProjectsComponent },
  { path: 'tecnology', component: TecnologyComponent },
  { path: 'display-alegra', component: DisplayAlegraComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
