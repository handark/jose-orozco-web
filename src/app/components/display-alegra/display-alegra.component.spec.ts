import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayAlegraComponent } from './display-alegra.component';

describe('DisplayAlegraComponent', () => {
  let component: DisplayAlegraComponent;
  let fixture: ComponentFixture<DisplayAlegraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayAlegraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayAlegraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
