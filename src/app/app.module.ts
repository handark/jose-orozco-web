import { BrowserModule } from '@angular/platform-browser';
import { NgModule,PLATFORM_ID, APP_ID, Inject  } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { 
        MatButtonModule, MatToolbarModule, MatMenuModule, 
        MatFormFieldModule, MatInputModule, MatSidenavModule,
        MatIconModule, MatListModule, MatGridListModule
      } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { DisplayAlegraComponent } from './components/display-alegra/display-alegra.component';
import { ContactComponent } from './components/contact/contact.component';
import { TecnologyComponent } from './components/tecnology/tecnology.component';
import { ExperienceProjectsComponent } from './components/experience-projects/experience-projects.component';
import { StudiesComponent } from './components/studies/studies.component';
import { isPlatformBrowser } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    DisplayAlegraComponent,
    ContactComponent,
    TecnologyComponent,
    ExperienceProjectsComponent,
    StudiesComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'jose-orozco-ng' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule, 
    MatToolbarModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatIconModule, MatListModule, MatGridListModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }

}
