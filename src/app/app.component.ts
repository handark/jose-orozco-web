import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  transition,
  trigger,
  query,
  style,
  animate,
  group,
  animateChild
} from '@angular/animations';
import {MatSidenav} from '@angular/material/sidenav';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('animacionPaginas', [
      transition('* => *', [
        query(
          ':enter',
          [style({ opacity: 0 })],
          { optional: true }
        ),
        query(
          ':leave',
           [style({ opacity: 1 }), animate('0.3s', style({ opacity: 0 }))],
          { optional: true }
        ),
        query(
          ':enter',
          [style({ opacity: 0 }), animate('0.3s', style({ opacity: 1 }))],
          { optional: true }
        )
      ])
    ])
  ]
})
export class AppComponent {

  @ViewChild('sidenav') sidenav: MatSidenav;

  title = 'Jose Luis Orozco';
  botonActivo:any = []

  constructor(private router: Router) {
    this.botonActivo = [false, false, false, false, false]
  }

  seccionA(seccion, index) {
    location.href = `#${seccion}`
    this.activarBotonLink(index)
  }

  linkA(link, index) {
    this.router.navigateByUrl(`/${link}`);
    this.activarBotonLink(index)
  }

  activarBotonLink(index) {
    this.botonActivo = [false, false, false, false, false]
    this.botonActivo[index] = true;
  }

  cerrarSideNav() {
    this.sidenav.close();
  }

}
